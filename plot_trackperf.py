#!/usr/bin/env python

# %% Package imports
import shark

import atlas.parquet as apq

import sys
import yaml
import numpy as np
import matplotlib.pyplot as plt
import matplotlib

import pyspark.sql.functions as f

from kkdata import dm
import kkplot

import lowmu.tools
import lowmu.plots

matplotlib.rcParams['savefig.format']='pdf'
# %% Prepare configuration
if sys.argv[0]=='ipykernel_launcher': # running in a notebook
    config='modes/mode_altgeo.yaml'
else:
    if len(sys.argv)!=2:
        print('usage: {} mode_config.yaml'.format(sys.argv[0]))
        sys.exit(1)
    config=sys.argv[1]

config = yaml.load(open(config), Loader=yaml.FullLoader)

title=config['title']
samples=config['samples']
prefix=config['prefix']

data=dm.DataManager(config['data'])
df=apq.concat_load([data[sample] for sample in samples],'all')

# %% Tracking detector acceptance
rdf = lowmu.tools.reco(df)
fdf = lowmu.tools.fake(df)
gdf = rdf.filter(rdf.goodMatch)

# %% Resolution plots
lowmu.plots.resolution(gdf, samples, data, title=title, prefix=f'{prefix}')

# %% Efficiency plots
lowmu.plots.efficiency(rdf, samples, data, title=title, prefix=f'{prefix}')

# %% Fake Rate vs pT
fhs,b=shark.histogram(fdf, df.track_pt, 100, 0, 10e3, groupby=['dsid','badMatch'])

fig, ax=plt.subplots(2,1,sharex=True,squeeze=True,gridspec_kw={'height_ratios':(2,1),'hspace':0.1})

ref=None
referr=None
for name in samples:
    dsid=data[name].dsid

    gh=fhs[(dsid, True )]
    bh=fhs[(dsid, False)]
    ah=gh+bh

    x,myf,xerr,err=kkplot.efficiency(b/1e3,ah,gh,label=data[name].title,ax=ax[0])

    if ref is None:
        ref=myf
        referr=err
    r=myf/ref
    ax[1].errorbar(x, r, xerr=xerr, yerr=r*np.sqrt((err/myf)**2+(referr/ref)**2), fmt='.')

ax[0].set_xlim(0,3)
kkplot.ticks(ax[0].xaxis, 0.5, 0.1)
ax[0].set_ylim(1e-6,1)
ax[1].set_ylim(0.8,1.2)
kkplot.ticks(ax[1].yaxis, 0.1, 0.02)
ax[1].set_xlabel('Track $p_{T}$ [GeV]')
ax[0].set_ylabel('Fake Rate')
ax[1].set_ylabel('Ratio')
ax[0].set_yscale('log')
ax[0].set_title(title)
ax[0].legend()

fig.tight_layout()
fig.savefig(f'plots/{prefix}_fake_pt')

# %% Fake Rate vs low pT
fhs,b=shark.histogram(fdf, df.track_pt, 200, 0, 10e3, groupby=['dsid','badMatch'])

fig, ax=kkplot.subplot_ratio()

ref=None
referr=None
for name in samples:
    dsid=data[name].dsid

    gh=fhs[(dsid, True )]
    bh=fhs[(dsid, False)]
    ah=gh+bh

    x,myf,xerr,err=kkplot.efficiency(b/1e3,ah,gh,label=data[name].title,ax=ax[0])

    if ref is None:
        ref=myf
        referr=err
    r=myf/ref
    ax[1].errorbar(x, r, xerr=xerr, yerr=r*np.sqrt((err/myf)**2+(referr/ref)**2), fmt='.')

ax[0].set_xlim(0,1)
kkplot.ticks(ax[0].xaxis, 0.1, 0.02)
ax[0].set_ylim(1e-6,1)
ax[1].set_ylim(0.8,1.2)
kkplot.ticks(ax[1].yaxis, 0.1, 0.02)
ax[1].set_xlabel('Track $p_{T}$ [GeV]')
ax[0].set_ylabel('Fake Rate')
ax[1].set_ylabel('Ratio')
ax[0].set_yscale('log')
ax[0].set_title(title)
ax[0].legend(ncol=2)

fig.tight_layout()
fig.savefig(f'plots/{prefix}_fake_lowpt')

# %%
