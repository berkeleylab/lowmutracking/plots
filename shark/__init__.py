import pyspark.sql.functions as f

import numpy as np

import itertools

def varbin(df, outname, col, bins, minval, maxval):
    """
    Define a new column, `outname`, that contains the bin index of `col`.
    """
    col=f.col(col) if type(col)==str else col
    width=(maxval-minval)/bins
    bins=np.arange(minval,maxval+width,width)
    newdf=df.withColumn(outname, f.floor((col-minval)/width))

    return newdf, bins

def histogram(df, col, bins, minval, maxval, groupby=[]):
    """
    Create a histogram of column `col` from dataframe `df`.
    """

    # Prepare the groupby bins
    if groupby is None:
        groupby=[]

    # Bin the data
    df,bins=varbin(df, 'bin', col, bins, minval, maxval)
    hist=df.groupby(groupby+['bin']).count().collect()

    # Create histograms
    hists=np.zeros(bins.shape[0]-1) if len(groupby)==0 else {}

    # Fill histograms
    for h in hist:
        if type(hists)==dict: # group of histograms
            # build up groupby key
            groupbykey=tuple(h[col] for col in groupby)

            # Get histogram
            if groupbykey not in hists:
                hists[groupbykey]=np.zeros(bins.shape[0]-1)
            vals=hists[groupbykey]
        else: # this is a single histogram
            vals=hists

        # Set the histogram contents
        if h['bin']==None or h['bin']<0 or vals.shape[0]<=h['bin']:
            continue # over/under flow
        vals[h['bin']]=h['count']

    # Fill empty groupby's with empty histograms
    if type(hists)==dict:
        keys=[set() for i in range(len(groupby))]

        for key in hists.keys():
            for i in range(len(keys)):
                keys[i].add(key[i])

        allkeys=itertools.product(*keys)
        for key in allkeys:
            if key not in hists:
                hists[key]=np.zeros(bins.shape[0]-1)


    # Done!
    return hists, bins