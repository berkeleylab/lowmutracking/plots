"""
Helpful tools for making tracking performance plots.
"""

def reco(df, pmatch=0.5):
    """
    Return a dataframe containing reconstrutable particles.
    
    The definition of reconstrutable is:
    - truth columns are valid
    - passed truth selection tool
    - highest ranking matched to a track

    Also defines a `goodMatch` column that contains whether the particle has
    been reconstructed. It requries that `track_truthMatchProb>pmatch`.
    """
    rdf = df.filter(df.hasTruth==1) \
            .filter(df.passedTruthSelection==1) \
            .filter(df.track_truthMatchRanking<1) \
            .withColumn("goodMatch", (df.passedTrackSelection==1)&(df.track_truthMatchProb>pmatch))
    return rdf

def fake(df, pmatch=0.5):
    """
    Return a dataframe containing fake tracks.
    
    The definition of fake track is:
    - track column is valid
    - passed track selection tool
    - truth column is valid

    Also defines a `badMatch` column that says whether the track is matched to
    a truth particle with `track_truthMatchProb<pmatch`.
    """
    fdf = df.filter(df.hasTrack==1) \
            .filter(df.passedTrackSelection==1) \
            .filter(df.hasTruth==1) \
            .withColumn("badMatch" , df.track_truthMatchProb<pmatch)
    return fdf
