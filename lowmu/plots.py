import numpy as np

import pyspark.sql.functions as f

import shark

import kkplot

def efficiency(rdf, samples, data, title='', prefix=''):
    """
    Make all efficiency plots, diced in different ways.

    Parameters:
     - rdf: dataframe with reconstructable particles
     - data: data manager with sample metadata
     - samples: list of samples to plot
     - title: title for figure
     - prefix: prefix for output
    """
    # %% Eff vs pT
    rhs,b=shark.histogram(rdf, rdf.truth_pt, 200, 0, 10e3, groupby=['dsid','goodMatch'])

    fig, ax=kkplot.subplot_ratio()

    refe=None
    referr=None
    for name in samples:
        gh=rhs[(data[name].dsid,True )]
        bh=rhs[(data[name].dsid,False)]
        ah=gh+bh

        x,mye,xerr,err=kkplot.efficiency(b/1e3,ah,gh,label=data[name].title, ax=ax[0])

        if refe is None:
            refe=mye
            referr=err
        ax[1].errorbar(x, mye-refe, xerr=xerr, yerr=np.sqrt(err**2+referr**2), fmt='.')

    ax[0].set_xlim(0,3)
    kkplot.ticks(ax[0].xaxis, 0.5, 0.1)
    ax[1].set_xlabel('Charged Particle $p_{T}$ [GeV]')
    ax[1].set_ylabel('Abs Change')
    kkplot.yeff(ax=ax[0])
    ax[1].set_ylim(-0.01,0.01)
    ax[0].set_title(title)
    ax[0].legend()

    fig.tight_layout()
    fig.savefig(f'plots/{prefix}_eff_pt')

    # %% Eff vs low pT
    rhs,b=shark.histogram(rdf, rdf.truth_pt, 200, 0,10e3, groupby=['dsid','goodMatch'])

    fig, ax=kkplot.subplot_ratio()

    refe=None
    referr=None
    for name in samples:
        gh=rhs[(data[name].dsid,True )]
        bh=rhs[(data[name].dsid,False)]
        ah=gh+bh

        x,mye,xerr,err=kkplot.efficiency(b/1e3,ah,gh,label=data[name].title, ax=ax[0])

        if refe is None:
            refe=mye
            referr=err
        ax[1].errorbar(x, mye-refe, xerr=xerr, yerr=np.sqrt(err**2+referr**2), fmt='.')

    ax[0].set_xlim(0,1)
    kkplot.ticks(ax[0].xaxis, 0.1, 0.02)
    ax[1].set_xlabel('Charged Particle $p_{T}$ [GeV]')
    ax[1].set_ylabel('Abs Change')
    kkplot.yeff(ax=ax[0])
    ax[1].set_ylim(-0.02,0.02)
    ax[0].set_title(title)
    ax[0].legend()

    fig.tight_layout()
    fig.savefig(f'plots/{prefix}_eff_lowpt')

    # %% Eff vs eta
    rdf_abseta=rdf.withColumn('truth_abseta',f.abs(rdf.truth_eta))
    rhs,b=shark.histogram(rdf_abseta, rdf_abseta.truth_abseta, 20, 0, 2.5, groupby=['dsid','goodMatch'])

    fig, ax=kkplot.subplot_ratio()

    refe=None
    referr=None
    for name in samples:
        gh=rhs[(data[name].dsid,True )]
        bh=rhs[(data[name].dsid,False)]
        ah=gh+bh

        x,mye,xerr,err=kkplot.efficiency(b,ah,gh,label=data[name].title, ax=ax[0])

        if refe is None:
            refe=mye
            referr=err
        ax[1].errorbar(x, mye-refe, xerr=xerr, yerr=np.sqrt(err**2+referr**2), fmt='.')

    ax[0].set_xlim(0,2.5)
    kkplot.ticks(ax[0].xaxis, 0.5, 0.1)
    ax[1].set_xlabel('Charged Particle $|\eta|$')
    ax[1].set_ylabel('Abs Change')
    kkplot.yeff(ax=ax[0])
    ax[1].set_ylim(-0.01,0.01)
    ax[0].set_title(title)
    ax[0].legend()

    fig.tight_layout()
    fig.savefig(f'plots/{prefix}_eff_abseta')

def resolution(gdf, samples, data, title='', prefix=''):
    """
    Make plots of resolutions.

    Parameters:
     - gdf: dataframe with reconstructed particles
     - data: data manager with sample metadata
     - samples: list of samples to plot
     - title: title for figure
     - prefix: prefix for output
    """

    plots=[
        {
            'colname':'d0',
            'hist' : {
                'xmin' : -3,
                'xmax' :  3,
                'nbins':100,
            },
            'fig' : {
                'xmin'  : -2,
                'xmax'  :  1,
                'xlabel':'$d_0$',
                'xunit' :'mm',
                'ymax'  : 0.1
            }
        },
        {
            'colname':'z0',
            'hist' : {
                'xmin' : -3,
                'xmax' :  3,
                'nbins':100,
            },
            'fig' : {
                'xmin'  : -2,
                'xmax'  :  1,
                'xlabel':'$z_0$',
                'xunit' :'mm',
                'ymax'  : 0.05
            }
        },
        {
            'colname':'qOverPt',
            'hist' : {
                'xmin' : -0.001,
                'xmax' :  0.001,
                'nbins':100,
            },
            'fig' : {
                'xmin'  : -0.001,
                'xmax'  :  0.0005,
                'xlabel':'$\\frac{q}{p_T}$'
            }
        }
    ]

    # Filter a specific pT range
    filt_gdf=gdf.filter(gdf.truth_pt<500)

    # %% Resolution plots
    for plot in plots:
        ihist=plot['hist']
        ifig =plot['fig' ]

        reso_df=filt_gdf.withColumn('reso_'+plot['colname'], f.col('truth_'+plot['colname'])-f.col('track_'+plot['colname']))
        vals, bins=shark.histogram(reso_df,'reso_'+plot['colname'], ihist['nbins'], ihist['xmin'], ihist['xmax'], groupby=['dsid'])

        fig, ax = kkplot.subplot_ratio()

        refvals=None
        for name in samples:
            myvals=vals[data[name].dsid,]
            myvals=myvals/myvals.sum()

            # histogram
            kkplot.hist(bins, myvals, ax=ax[0], label=data[name].title, histtype='step')
    
            # ratio
            if refvals is None:
                refvals=myvals
            kkplot.hist(bins, myvals/refvals, ax=ax[1], histtype='step')

        ax[0].set_xlim(ifig['xmin'],ifig['xmax'])
        ax[0].set_ylim(0,ifig.get('ymax',None))
        ax[0].set_ylabel('a.u.')

        xlabel='truth-reco '+ifig['xlabel']
        if 'xunit' in ifig:
            xlabel+= ' ['+ifig['xunit']+']'
        ax[1].set_xlabel(xlabel)
        ax[1].set_ylim(0.9,1.1)
        ax[1].set_ylabel('ratio')

        ax[0].set_title(f'{title}, $p_{{T,truth}}<500$ MeV')
        ax[0].legend(loc='upper left')

        fig.tight_layout()
        fig.savefig(f'plots/{prefix}_reso_'+plot['colname'])
