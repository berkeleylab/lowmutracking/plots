import pyspark.sql.functions as f

import glob

class TruthToReco:
    """
    Sample loader for output files produced using the ATLAS tracking validation
    package.
    """
    def __init__(self, dsid, path, sql=None):
        """
        Parameters:
         - path (str): path containing LCParquet's output, wildcards allowed
         - dsid (int): DSID to associate with the sample.
         - sql (SQLContext): Spark context to use for processing
        """
        self.path=path
        self.dsid=dsid

        self.sql=sql
        self._loaded={} # Cache loaded dataframes

    def load(self,name):
        """
        Load the contents of `path/name.parquet` where `path` is the sample path
        given in `__init__` and `name` is the requested dataframe.
        """

        if name in self._loaded:
            return self._loaded[name]

        paths=[]
        paths+=glob.glob('{}/{}.parquet'.format(self.path, name))
        if len(paths)==0:
            print('No match for {}/{}.parquet'.format(self.path, name))

        df=self.sql.read.parquet(*paths)
        if self.dsid!=None:
            df=df.withColumn('dsid', f.lit(self.dsid))
        self._loaded[name] = df

        return df

    def load_all(self):
        df=self.load('TruthToReco')

        df=df.withColumn('truth_abseta', f.abs(df.truth_eta))
        df=df.withColumn('truth_abspdgId', f.abs(df.truth_pdgId))

        return df

def concat_load(samples, branchset):
    """
    Load a dataframe from multiple samples into one.

    Parameters:
        - samples: List of samples
        - branchset: Dataframe to load (ie: calls `LCParquet.load_{branchset}`)
    """
    # Load dataframes into lists
    load_func=f'load_{branchset}'
    if hasattr(samples[0],load_func):
        mrg=[getattr(sample,load_func)() for sample in samples]
    else:
        mrg=[sample.load(branchset) for sample in samples]

    # Concatenate
    df=mrg.pop()
    for adf in mrg:
        df=df.union(adf)
    return df
