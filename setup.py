#!/usr/bin/env python

import setuptools

setuptools.setup(
    name='lowmuplots',
    version='0.0.1',
    description='Plotting code for low pileup tracking studies.',
    install_requires=[
        'pyspark'
      ],
    packages=['atlas','shark']
)
